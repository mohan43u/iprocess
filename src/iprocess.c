/* 
 * Copyright (c) <2012>, Mohan R <mohan43u@gmail.com>
 * All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 * 
 *   1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 *   of the authors and should not be interpreted as representing official policies,
 *   either expressed or implied, of the FreeBSD Project.
 */

#include <iprocess.h>

static void iprocess_getopt(int argc, char *argv[], GHashTable *arguments)
{
  gchar opt = '\0';
  struct option longopts[] = {{"dirs", 1, NULL, 'd'},
			      {"masks", 1, NULL, 'm'},
			      {"nthreads", 1, NULL, 'n'},
			      {"help", 0, NULL, 'h'}};
  gchar usage[] = "[usage] iprocess "
    "-d|--dirs DIR1[,DIR2,..] "
    "[-m|--masks MASK1,MASK2,...] "
    "[-n|--nthreads NUM] "
    "[-h|--help] -- commandline\n";
  gchar *commandline = NULL;
  gint32 nodirs = 1;
    
  if(argc <2)
    {
      g_printerr("%s", usage);
      exit(EXIT_FAILURE);
    }

  while((opt = getopt_long(argc, argv, "d:m:n:h", longopts, NULL)) != -1)
    {
      switch(opt)
	{
	case 'd':
	  {
	    nodirs = 0;
	    g_hash_table_insert(arguments, "dirs", optarg);
	  }
	  break;
	case 'm':
	  {
	    g_hash_table_insert(arguments, "masks", optarg);
	  }
	  break;
	case 'n':
	  {
	    g_hash_table_insert(arguments, "nthreads", optarg);
	  }
	  break;
	case 'h':
	  {
	    g_print("%s", usage);
	    exit(EXIT_SUCCESS);
	  }
	default:
	  {
	    g_print("iprocess_getopt(): unknown option '%c'\n", opt);
	    exit(EXIT_FAILURE);
	  }
	}
    }

  if(nodirs)
    {
      g_printerr("iprocess_getopt(): atleast one DIR must be provided\n");
      g_printerr("%s", usage);
      exit(EXIT_FAILURE);
    }

  commandline = g_strjoinv(" ", &argv[optind]);
  g_hash_table_insert(arguments, "commandline", commandline);
}

static GSList* iprocess_getpaths(GHashTable *arguments)
{
  gchar **dirs = g_strsplit_set((gchar *) g_hash_table_lookup(arguments, "dirs"), ",", 0);
  GString *path = NULL;
  GSList *paths = NULL;
  gint32 iter = 0;

  while(dirs[iter] != NULL)
    {
      path = g_string_new(dirs[iter]);
      paths = g_slist_append(paths, path);
      iter++;
    }

  return paths;
}

static gint32 iprocess_getmask(GHashTable *arguments)
{
  gchar *maskslist = g_hash_table_lookup(arguments, "masks");
  gchar **masks = NULL;
  gint32 mask = 0;
  gint32 iter = 0;

  if(maskslist == NULL)
    return IP_IN_ALL;

  masks = g_strsplit_set(maskslist, ",", 0);
  while(masks[iter] != NULL)
    {
      mask = mask | (g_strcmp0(IP_IN_ACCESS, masks[iter]) == 0 ? IN_ACCESS : 0);
      mask = mask | (g_strcmp0(IP_IN_ATTRIB, masks[iter]) == 0 ? IN_ATTRIB : 0);
      mask = mask | (g_strcmp0(IP_IN_CLOSE_WRITE, masks[iter]) == 0 ? IN_CLOSE_WRITE : 0);
      mask = mask | (g_strcmp0(IP_IN_CLOSE_NOWRITE, masks[iter]) == 0 ? IN_CLOSE_NOWRITE : 0);
      mask = mask | (g_strcmp0(IP_IN_CREATE, masks[iter]) == 0 ? IN_CREATE : 0);
      mask = mask | (g_strcmp0(IP_IN_DELETE, masks[iter]) == 0 ? IN_DELETE : 0);
      mask = mask | (g_strcmp0(IP_IN_DELETE_SELF, masks[iter]) == 0 ? IN_DELETE_SELF : 0);
      mask = mask | (g_strcmp0(IP_IN_MODIFY, masks[iter]) == 0 ? IN_MODIFY : 0);
      mask = mask | (g_strcmp0(IP_IN_MOVE_SELF, masks[iter]) == 0 ? IN_MOVE_SELF : 0);
      mask = mask | (g_strcmp0(IP_IN_MOVED_FROM, masks[iter]) == 0 ? IN_MOVED_FROM : 0);
      mask = mask | (g_strcmp0(IP_IN_MOVED_TO, masks[iter]) == 0 ? IN_MOVED_TO : 0);
      mask = mask | (g_strcmp0(IP_IN_OPEN, masks[iter]) == 0 ? IN_OPEN : 0);

      iter++;
    }

  return(mask);
}
  
int main(int argc, char *argv[])
{
  GHashTable *arguments = g_hash_table_new(g_str_hash, g_str_equal);
  GHashTable *watchlist = g_hash_table_new(NULL, NULL);
  GThreadPool *pool = NULL;
  GString *commandline = NULL;
  GSList *paths = NULL;
  gint32 mask = 0;
  gint64 nthreads = MAXTHREADS;
  gint32 ihandle = -1;

  iprocess_getopt(argc, argv, arguments);
  commandline = g_string_new(g_hash_table_lookup(arguments, "commandline"));
  paths = iprocess_getpaths(arguments);
  mask = iprocess_getmask(arguments);
  ihandle = iprocess_init(paths, mask, watchlist);
  if(g_hash_table_lookup(arguments, "nthreads") != NULL)
    nthreads = g_ascii_strtoll(g_hash_table_lookup(arguments, "nthreads"), NULL, 10);
  pool = iprocess_thread_pool_init(nthreads);
  iprocess_listen_inotify(ihandle, watchlist, commandline, pool);

  g_hash_table_remove_all(arguments);
  g_hash_table_remove_all(watchlist);
  g_slist_free_full(paths, (GDestroyNotify) g_string_free);
  return(0);
}
