/* 
 * Copyright (c) <2012>, Mohan R <mohan43u@gmail.com>
 * All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 * 
 *   1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 *   of the authors and should not be interpreted as representing official policies,
 *   either expressed or implied, of the FreeBSD Project.
 */

#define _GNU_SOURCE
#ifndef __IPROCESS__
#define __IPROCESS__

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <glib.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <getopt.h>

#define MAXTHREADS               1
#define EVENTARRAYSIZE           1024
#define IP_IN_ACCESS             "IN_ACCESS"
#define IP_IN_ATTRIB             "IN_ATTRIB"
#define IP_IN_CLOSE_WRITE        "IN_CLOSE_WRITE"
#define IP_IN_CLOSE_NOWRITE      "IN_CLOSE_NOWRITE"
#define IP_IN_CREATE             "IN_CREATE"
#define IP_IN_DELETE             "IN_DELETE"
#define IP_IN_DELETE_SELF        "IN_DELETE_SELF"
#define IP_IN_MODIFY             "IN_MODIFY"
#define IP_IN_MOVE_SELF          "IN_MOVE_SELF"
#define IP_IN_MOVED_FROM         "IN_MOVED_FROM"
#define IP_IN_MOVED_TO           "IN_MOVED_TO"
#define IP_IN_OPEN               "IN_OPEN"
#define IP_IN_ALL                IN_ACCESS|IN_ATTRIB|IN_CLOSE_WRITE|IN_CLOSE_NOWRITE|IN_CREATE|IN_DELETE|IN_DELETE_SELF|IN_MODIFY|IN_MOVE_SELF|IN_MOVED_FROM|IN_MOVED_TO|IN_OPEN

gint32 iprocess_init(GSList *paths, gint32 mask, GHashTable *watchlist);
void iprocess_listen_inotify(gint32 ihandle, GHashTable *watchlist, GString *commandline, GThreadPool *pool);
GThreadPool* iprocess_thread_pool_init(gint64 nthreads);
void iprocess_thread_pool_execute(GThreadPool *pool, gchar *commandline);
#endif /* __IPROCESS__ */
