/* 
 * Copyright (c) <2012>, Mohan R <mohan43u@gmail.com>
 * All rights reserved.
 * 
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 * 
 *   1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 *   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 *   of the authors and should not be interpreted as representing official policies,
 *   either expressed or implied, of the FreeBSD Project.
 */

#include <iprocess.h>

gint32 iprocess_init(GSList *paths, gint32 mask, GHashTable *watchlist)
{
  gint32 ihandle = -1;
  gint32 iwhandle = -1;
  gchar *errorstring = NULL;
  GString *path = NULL;
  GSList *iter = NULL;

  ihandle = inotify_init();
  if(ihandle == -1)
    {
      g_printerr("iprocess_init(): %s [code:%d]\n", strerror(errno), errno);
      exit(EXIT_FAILURE);
    }

  iter = paths;
  while(iter)
    {
      path = (GString *) iter->data;
      iwhandle = inotify_add_watch(ihandle, path->str, mask);
      if(iwhandle == -1)
	{
	  g_printerr("iprocess_init(): %s [code:%d]\n", strerror(errno), errno);
	  exit(EXIT_FAILURE);
	}
      g_hash_table_insert(watchlist, (gpointer) (gint64) iwhandle, (gpointer) path);
      iter = g_slist_next(iter);
    }

  return ihandle;
}

static void iprocess_replace(GString *commandline, gchar *field, gchar *value)
{
  gchar *orig = commandline->str;
  gchar *needle = NULL;
  gsize fpartlen = 0;
  gchar *fpart = NULL;
  gchar *spart = NULL;
  gsize fieldlen = 0;

  while(TRUE)
    {
      orig = commandline->str;
      needle = strstr(orig, field);
      if(needle == NULL)
	break;
      fpartlen = needle - orig;
      fpart = strndup(orig, fpartlen);
      fieldlen = strlen(field);
      spart = strdup(&needle[fieldlen]);
      g_string_printf(commandline, "%s%s%s", fpart, value, spart);
    }

  g_free(fpart);
  g_free(spart);
}

static gchar* iprocess_getmask(gint32 mask)
{
  GString *strmask = g_string_new(g_strdup_printf("%d", mask));

  if(IN_ACCESS & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_ACCESS);
  if(IN_ATTRIB & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_ATTRIB);
  if(IN_CLOSE_WRITE & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_CLOSE_WRITE);
  if(IN_CLOSE_NOWRITE & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_CLOSE_NOWRITE);
  if(IN_CREATE & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_CREATE);
  if(IN_DELETE & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_DELETE);
  if(IN_DELETE_SELF & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_DELETE_SELF);
  if(IN_MODIFY & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_MODIFY);
  if(IN_MOVE_SELF & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_MOVE_SELF);
  if(IN_MOVED_FROM & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_MOVED_FROM);
  if(IN_MOVED_TO & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_MOVED_TO);
  if(IN_OPEN & mask)
    g_string_append_printf(strmask, "|%s", IP_IN_OPEN);

  return strmask->str;
}

static void iprocess_execute(struct inotify_event *event, GString *path, GString *commandline, GThreadPool *pool)
{
  gchar *wd = g_strdup_printf("%d", event->wd);
  gchar *mask = iprocess_getmask(event->mask);
  gchar *cookie = g_strdup_printf("%d", event->cookie);
  gchar *len = g_strdup_printf("%d", event->len);
  gchar *fullname = g_strdup_printf("%s/%s", (path == NULL ? "" : path->str), event->name);
  gchar *name = g_strdup_printf("%s", event->name);

  iprocess_replace(commandline, "[wd]", wd); 
  iprocess_replace(commandline, "[mask]", mask);
  iprocess_replace(commandline, "[cookie]", cookie);
  iprocess_replace(commandline, "[len]", len);
  iprocess_replace(commandline, "[fullname]", fullname);
  iprocess_replace(commandline, "[name]", name);

  g_print("%s\n", commandline->str);
  if(event->wd > 0)
    iprocess_thread_pool_execute(pool, g_strdup(commandline->str));

  g_free(wd);
  g_free(mask);
  g_free(cookie);
  g_free(len);
  g_free(fullname);
  g_free(name);
}

void iprocess_listen_inotify(gint32 ihandle, GHashTable *watchlist, GString *commandline_t, GThreadPool *pool)
{
  struct inotify_event *event = g_try_new0(struct inotify_event, EVENTARRAYSIZE);
  GString *commandline = NULL;
  GString *path = NULL;
  gint32 length = 0;
  gint32 iter = 0;

  while(TRUE)
    {
      memset(event, 0, sizeof(struct inotify_event) * EVENTARRAYSIZE);
      length = read(ihandle, event, sizeof(struct inotify_event) * EVENTARRAYSIZE);
      if( length == -1)
	{
	  if(errno == EINTR)
	    continue;
	  g_printerr("iprocess_listen_inotify(): %s [code:%d]\n", strerror(errno),errno);
	  exit(EXIT_FAILURE);
	}

      iter = 0;
      while(iter < length)
	{
	  path = g_hash_table_lookup(watchlist, (gpointer) (gint64) event[iter].wd);
	  commandline = g_string_new(commandline_t->str);
	  iprocess_execute(&event[iter], path, commandline, pool);
	  g_string_free(commandline, TRUE);
	  iter = (iter + sizeof(struct inotify_event));
	}
    }
}
